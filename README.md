# alipay-plugin

#### 介绍
支付宝商户代扣方法集成，包含扣款、退款、授权、解约、查询等等功能。

#### 软件架构

1. Spring boot 2.1.6.RELEASE
2. swagger2 2.9.2

#### 使用说明

1. 在controller文件夹中TestController类为控制器示例，只演示了基本必要参数
2. 在apipay_api_file文件夹中有各种必要支付宝api文档
3. alipay-config.properties是支付宝配置文件

#### 参与贡献

1. XuYang