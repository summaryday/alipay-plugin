package com.alipay.zxy.alipayplugin.service;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import com.alipay.zxy.alipayplugin.config.AlipayConfig;
import lombok.extern.slf4j.Slf4j;

import javax.xml.transform.Result;

/**
 * @author zxy
 */
@Slf4j
public class BaseService {
    AlipayRequest getAlipayRequest(AlipayRequest alipayRequest, AlipayConfig alipayConfig, AlipayObject alipayModel) {
        //设置业务参数，alipayModel为前端发送的请求信息，开发者需要根据实际情况填充此类
        alipayRequest.setBizModel(alipayModel);
        alipayRequest.setReturnUrl(alipayConfig.getReturnUrl());
        alipayRequest.setNotifyUrl(alipayConfig.getNotifyUrl());
        log.info("支付宝支付请求参数alipayRequest={}", JSONObject.toJSONString(alipayRequest));
        return alipayRequest;
    }
}
