package com.alipay.zxy.alipayplugin.message.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
public class AlipayUserAgreementQueryDTO implements Serializable {
    private String agreementNo;
    private String externalLogonId;
    private String invalidTime;
    private String alipayUserId;
}
