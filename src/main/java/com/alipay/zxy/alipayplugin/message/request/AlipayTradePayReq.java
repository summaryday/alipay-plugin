package com.alipay.zxy.alipayplugin.message.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
@ApiModel(value = "支付宝扣款请求model")
public class AlipayTradePayReq implements Serializable {
    @ApiModelProperty(value = "支付宝用户签约号", example = "20195425547111111111",required = true)
    private String agreementNo;
    @ApiModelProperty(value = "商品名称", example = "测试商品",required = true)
    private String subject;
    @ApiModelProperty(value = "扣款金额", example = "1.00",required = true)
    private String totalAmount;
    @ApiModelProperty(value = "商户订单号(根据业务需求商户自己生成)", example = "0000000000000011",required = true)
    private String outTradeNo;
}
