package com.alipay.zxy.alipayplugin.message.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
@ApiModel(value = "支付宝签约请求model")
public class AlipayPageSignReq implements Serializable {
    @ApiModelProperty(value = "商户用户Id")
    private String externalLogonId;
    @ApiModelProperty(value = "商户app跳转返回地址")
    private String returnUrl;
}
