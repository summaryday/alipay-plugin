package com.alipay.zxy.alipayplugin.message.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "支付宝签约查询请求model")
public class AlipayQuerySignReq implements Serializable {
    @ApiModelProperty(value = "用户签约号")
    private String agreementNo;
}
