package com.alipay.zxy.alipayplugin.message.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
@ApiModel(value = "支付宝交易查询请求model")
public class AlipayTradeQueryReq implements Serializable {
    @ApiModelProperty(value = "商户订单号")
    private String outTradeNo;
    @ApiModelProperty(value = "支付宝交易订单号")
    private String tradeNo;
}
