package com.alipay.zxy.alipayplugin.message.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zxy
 */
@Data
@ApiModel(value = "支付宝扣款响应model")
public class AlipayTradePayDTO implements Serializable {
    @ApiModelProperty(value = "买家支付宝用户id", example = "2088202954065786")
    private String buyerUserId;
    @ApiModelProperty(value = "交易时间(时间戳 单位:秒)", example = "1561702962")
    private Long gmtPayment;
    @ApiModelProperty(value = "商户订单号，保证商户端不重复", example = "201503200101001")
    private String outTradeNo;
    @ApiModelProperty(value = "交易金额", example = "88.88")
    private String totalAmount;
    @ApiModelProperty(value = "支付宝订单号", example = "2088202954065786")
    private String tradeNo;
    @ApiModelProperty(value = "实际金额", example = "88.88")
    private String receiptAmount;
}
