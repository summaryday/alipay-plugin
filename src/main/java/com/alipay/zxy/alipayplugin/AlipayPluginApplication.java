package com.alipay.zxy.alipayplugin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlipayPluginApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlipayPluginApplication.class, args);
    }

}
