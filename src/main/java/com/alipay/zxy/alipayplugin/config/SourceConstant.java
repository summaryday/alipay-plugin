package com.alipay.zxy.alipayplugin.config;

/**
 * @author zxy
 */
public class SourceConstant {
    public static final String ALIPAY_APP_AUTH = "alipay_app_auth";
    public static final String ALIPAY_WALLET = "alipay_wallet";
}
