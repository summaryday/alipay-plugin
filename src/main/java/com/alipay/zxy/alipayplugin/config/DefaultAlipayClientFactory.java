package com.alipay.zxy.alipayplugin.config;

import com.alipay.api.DefaultAlipayClient;

/**
 * @author zxy
 */
public class DefaultAlipayClientFactory {

    private DefaultAlipayClientFactory() {
    }

    private static DefaultAlipayClient instance;

    public static synchronized DefaultAlipayClient getInstance(AlipayConfig alipayConfig) {
        if (instance == null) {
            instance = new DefaultAlipayClient(
                    alipayConfig.getAlipayGatewayUrl(),
                    alipayConfig.getAppId(),
                    alipayConfig.getRsaPrivateKey(),
                    alipayConfig.getFormat(),
                    alipayConfig.getCharset(),
                    alipayConfig.getAlipayRsaPublicKey(),
                    alipayConfig.getSignType());
        }
        return instance;
    }
}