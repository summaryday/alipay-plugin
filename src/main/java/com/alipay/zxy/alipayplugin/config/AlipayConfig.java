package com.alipay.zxy.alipayplugin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 阿里云配置文件
 *
 * @author 周徐阳
 * @date 2019-06-05
 */
@Data
@Component
@PropertySource({"classpath:alipay-config.properties"})
public class AlipayConfig implements Serializable {
    public static final String ALIPAY_SYSTEM = "alipay_system";
    public static final String ALIPAY_VERSION = "alipay_system_java_20190327";
    /**
     * 商户appid
     */
    @Value(value = "${APP_ID}")
    private String appId;
    /**
     * 私钥 pkcs8格式
     */
    @Value(value = "${RSA2_PRIVATE_KEY}")
    private String rsaPrivateKey;
    /**
     * 公钥
     */
    @Value(value = "${RSA2_PUBLIC_KEY}")
    private String rsaPublicKey;
    /**
     * 支付宝公钥
     */
    @Value(value = "${ALIPAY_RSA2_PUBLIC_KEY}")
    private String alipayRsaPublicKey;
    /**
     * 支付宝网关
     */
    @Value(value = "${ALIPAY_GATEWAY_URL}")
    private String alipayGatewayUrl;
    /**
     * 服务器异步回调地址
     */
    @Value(value = "${NOTIFY_URL}")
    private String notifyUrl;
    /**
     * 服务器同步回跳地址
     */
    @Value(value = "${RETURN_URL}")
    private String returnUrl;
    /**
     * 沙箱买家账号
     */
    @Value(value = "${SANDBOX_BUYER_EMAIL}")
    private String sandboxBuyerEmail;
    /**
     * 沙箱买家账号登录密码
     */
    @Value(value = "${SANBOX_BUYER_LOGON_PWD}")
    private String sanboxBuyerLogonPwd;
    /**
     * 沙箱买家账号支付密码
     */
    @Value(value = "${SANBOX_BUYER_PAY_PWD}")
    private String sanboxBuyerPayPwd;
    /**
     * 沙箱卖家账号PID
     */
    @Value(value = "${SANDBOX_SELLER_ID}")
    private String sandboxSellerId;
    /**
     * 沙箱卖家账号
     */
    @Value(value = "${SANDBOX_SELLER_EMAIL}")
    private String sandboxSellerEmail;
    /**
     * 沙箱卖家登录密码
     */
    @Value(value = "${SANDBOX_SELLER_LOGON_PWD}")
    private String sandboxSellerLogonPwd;
    /**
     * 编码
     */
    @Value(value = "${CHARSET}")
    private String charset;
    /**
     * 返回格式
     */
    @Value(value = "${FORMAT}")
    private String format;
    /**
     * 签名方式
     */
    @Value(value = "${SIGN_TYPE}")
    private String signType;
}